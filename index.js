const { pagingConduitFetch, indexedArgs, scopedIndexedArgs, flatten } = require('./utility.js');

const getTaskTransactionData = async (url, token, taskIds) => {
    const data = await pagingConduitFetch(
        `${url}/api/maniphest.gettasktransactions`,
        {
          'api.token': token,
          ...indexedArgs('ids', taskIds)
        }
    )
    return data;
};

function isColumnTransaction( transaction ) {
  return transaction.transactionType == 'core:columns'
}

function columnTransactionReducer(accum, transaction) {
  const taskID = transaction.taskID;
  const authorPHID = transaction.authorPHID;
  const date = transaction.dateCreated;
  transaction.newValue.forEach(newValue => {
    const toColumnPHID = newValue.columnPHID;
    const boardPHID = newValue.boardPHID;
    const fromColumnPHIDs = Object.keys(newValue.fromColumnPHIDs);
    if (fromColumnPHIDs.length == 0) {
      fromColumnPHIDs.push(null)
    }
    fromColumnPHIDs.forEach(fromColumnPHID => {
      accum.push({
        taskID,
        toColumnPHID,
        fromColumnPHID,
        authorPHID,
        date,
        boardPHID
      })
    })
  })
  return accum;
}

async function getColumnMovementsForTasks( taskIds, url, token ) {
  const data = await getTaskTransactionData(url, token, taskIds);
  const columnMovements = flatten(Object.values(data))
    .filter(isColumnTransaction)
    .reduce(columnTransactionReducer, []);
  return columnMovements;
}

module.exports = {
  getColumnMovementsForTasks
};
