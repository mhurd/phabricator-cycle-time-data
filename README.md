Work-in-progress project to coax task cycle-time metrics from Phabricator API data

----

Currently implements:

`getColumnMovementsForTasks()`

This extracts task column movement info from the `maniphest.gettasktransactions` endpoint. It returns data that looks like this:

```json
[
  {
    taskID: '148805',
    toColumnPHID: 'PHID-PCOL-e3ppe2bl2awg2e5nqj23',
    fromColumnPHID: 'PHID-PCOL-hkfii3we6ugzz2uu3goi',
    authorPHID: 'PHID-USER-ohzzl3maortma7y4znpb',
    date: '1683581173',
    boardPHID: 'PHID-PROJ-izvc7cprybmkosiqgfra'
  },
  {
    taskID: '148805',
    toColumnPHID: 'PHID-PCOL-hkfii3we6ugzz2uu3goi',
    fromColumnPHID: 'PHID-PCOL-e3ppe2bl2awg2e5nqj23',
    authorPHID: 'PHID-USER-ohzzl3maortma7y4znpb',
    date: '1683581152',
    boardPHID: 'PHID-PROJ-izvc7cprybmkosiqgfra'
  },
  {
    taskID: '148805',
    toColumnPHID: 'PHID-PCOL-hmy4l7f3ix5e2c2kdzf7',
    fromColumnPHID: 'PHID-PCOL-2nsjommzkgsxu526o6wi',
    authorPHID: 'PHID-USER-fn7qnpccfbitivgtw2rt',
    date: '1636569797',
    boardPHID: 'PHID-PROJ-uier7rukzszoewbhj7ja'
  },
  {
    taskID: '280999',
    toColumnPHID: 'PHID-PCOL-hkfii3we6ugzz2uu3goi',
    fromColumnPHID: 'PHID-PCOL-e3ppe2bl2awg2e5nqj23',
    authorPHID: 'PHID-USER-ohzzl3maortma7y4znpb',
    date: '1683591332',
    boardPHID: 'PHID-PROJ-izvc7cprybmkosiqgfra'
  },
  {
    taskID: '280999',
    toColumnPHID: 'PHID-PCOL-e3ppe2bl2awg2e5nqj23',
    fromColumnPHID: 'PHID-PCOL-hkfii3we6ugzz2uu3goi',
    authorPHID: 'PHID-USER-ohzzl3maortma7y4znpb',
    date: '1683581172',
    boardPHID: 'PHID-PROJ-izvc7cprybmkosiqgfra'
  }
]
```

Current thinking is to try using such data to create a table in a datastore which we could query against. More coming soon...

----

Run `make test` to spin it up and run tests.

Run `make sample` to spin it up and do a single sample call. 

----

`PHABRICATOR_URL` and `CONDIUT_TOKEN` must be set in the Dockerfile file first.