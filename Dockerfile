FROM node:alpine

ENV PHABRICATOR_URL "https://phabricator.wikimedia.org"
ENV CONDUIT_TOKEN ""

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

CMD ["tail", "-f", "/dev/null"]
