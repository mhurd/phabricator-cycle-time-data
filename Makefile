.DEFAULT: freshinstall
.PHONY: freshinstall
freshinstall:
	-make stop
	-make remove
	make prepare

.PHONY: prepare
prepare:
	docker build -t phabricator-cycle-time-data .

.PHONY: remove
remove:
	docker rm phabricator-cycle-time-data

.PHONY: stop
stop:
	docker stop phabricator-cycle-time-data

.PHONY: test
test:
	make
	docker run -it --rm phabricator-cycle-time-data /bin/sh -c "npm test"

.PHONY: sample
sample:
	make
	docker run -it --rm phabricator-cycle-time-data \
	    /bin/sh -c "node -e '\
	        (async () => { \
	            const result = await require(\"./index.js\") \
	                .getColumnMovementsForTasks( \
	                    [280999, 148805], \
	                    process.env.PHABRICATOR_URL, \
	                    process.env.CONDUIT_TOKEN \
	                ); \
	            console.log(JSON.stringify(result, null, 2)); \
	        })()'"
