const { getColumnMovementsForTasks } = require('./index');

jest.setTimeout(30000)

const expectedEntries = [
  {
    taskID: '148805',
    toColumnPHID: 'PHID-PCOL-e3ppe2bl2awg2e5nqj23',
    fromColumnPHID: 'PHID-PCOL-hkfii3we6ugzz2uu3goi',
    authorPHID: 'PHID-USER-ohzzl3maortma7y4znpb',
    date: '1683581173',
    boardPHID: 'PHID-PROJ-izvc7cprybmkosiqgfra'
  },
  {
    taskID: '148805',
    toColumnPHID: 'PHID-PCOL-hkfii3we6ugzz2uu3goi',
    fromColumnPHID: 'PHID-PCOL-e3ppe2bl2awg2e5nqj23',
    authorPHID: 'PHID-USER-ohzzl3maortma7y4znpb',
    date: '1683581152',
    boardPHID: 'PHID-PROJ-izvc7cprybmkosiqgfra'
  },
  {
    taskID: '148805',
    toColumnPHID: 'PHID-PCOL-hmy4l7f3ix5e2c2kdzf7',
    fromColumnPHID: 'PHID-PCOL-2nsjommzkgsxu526o6wi',
    authorPHID: 'PHID-USER-fn7qnpccfbitivgtw2rt',
    date: '1636569797',
    boardPHID: 'PHID-PROJ-uier7rukzszoewbhj7ja'
  },
  {
    taskID: '148805',
    toColumnPHID: 'PHID-PCOL-e3ppe2bl2awg2e5nqj23',
    fromColumnPHID: 'PHID-PCOL-tkydygo6emex7eml3xno',
    authorPHID: 'PHID-USER-5ewyncd6mpezaymyxfal',
    date: '1619202908',
    boardPHID: 'PHID-PROJ-izvc7cprybmkosiqgfra'
  },
  {
    taskID: '148805',
    toColumnPHID: 'PHID-PCOL-nmjt6iq6u2a2x3gxfr4j',
    fromColumnPHID: 'PHID-PCOL-v34exc7ht5ollou3zmih',
    authorPHID: 'PHID-USER-hgn5uw2jafgjgfvxibhh',
    date: '1613217132',
    boardPHID: 'PHID-PROJ-kfrrtvyn66ou2iq4y4ai'
  },
  {
    taskID: '148805',
    toColumnPHID: 'PHID-PCOL-v34exc7ht5ollou3zmih',
    fromColumnPHID: 'PHID-PCOL-mxr5zx3mjdzcdk3fmk3w',
    authorPHID: 'PHID-USER-fn7qnpccfbitivgtw2rt',
    date: '1597178340',
    boardPHID: 'PHID-PROJ-kfrrtvyn66ou2iq4y4ai'
  },
  {
    taskID: '148805',
    toColumnPHID: 'PHID-PCOL-cxdgyvzzlshj3ubjyffr',
    fromColumnPHID: 'PHID-PCOL-cxdgyvzzlshj3ubjyffr',
    authorPHID: 'PHID-USER-mzjfuzwqhxgtksmcqpn3',
    date: '1481328265',
    boardPHID: 'PHID-PROJ-fmzhf65uexhr2bhebrqr'
  },
  {
    taskID: '148805',
    toColumnPHID: 'PHID-PCOL-cxdgyvzzlshj3ubjyffr',
    fromColumnPHID: 'PHID-PCOL-clzpiqoh6k524c3t7ezn',
    authorPHID: 'PHID-USER-mzjfuzwqhxgtksmcqpn3',
    date: '1481327783',
    boardPHID: 'PHID-PROJ-fmzhf65uexhr2bhebrqr'
  },
  {
    taskID: '148805',
    toColumnPHID: 'PHID-PCOL-plsngegjuwikmxt6opfn',
    fromColumnPHID: 'PHID-PCOL-prldd6pm4baeuiz3kh2k',
    authorPHID: 'PHID-USER-ev5njt6ynenwaa6whrjc',
    date: '1478110012',
    boardPHID: 'PHID-PROJ-4y5gw5adxnh7eb5t6mqq'
  },
  {
    taskID: '148805',
    toColumnPHID: 'PHID-PCOL-osttotml3kldrof6cbx5',
    fromColumnPHID: 'PHID-PCOL-yjg66fqim6rp5aho7jis',
    authorPHID: 'PHID-USER-mzjfuzwqhxgtksmcqpn3',
    date: '1477676199',
    boardPHID: 'PHID-PROJ-ipmzo4jausp73zxxcft2'
  },
  {
    taskID: '148805',
    toColumnPHID: 'PHID-PCOL-fqyiijpn3dfexvvxpsmk',
    fromColumnPHID: 'PHID-PCOL-y6mgqvnmyirvwitvjsik',
    authorPHID: 'PHID-USER-mzjfuzwqhxgtksmcqpn3',
    date: '1477505870',
    boardPHID: 'PHID-PROJ-pb7r4scsjbt7mvktvb6g'
  },
  {
    taskID: '280999',
    toColumnPHID: 'PHID-PCOL-e3ppe2bl2awg2e5nqj23',
    fromColumnPHID: 'PHID-PCOL-hkfii3we6ugzz2uu3goi',
    authorPHID: 'PHID-USER-ohzzl3maortma7y4znpb',
    date: '1683581172',
    boardPHID: 'PHID-PROJ-izvc7cprybmkosiqgfra'
  },
  {
    taskID: '280999',
    toColumnPHID: 'PHID-PCOL-hkfii3we6ugzz2uu3goi',
    fromColumnPHID: 'PHID-PCOL-e3ppe2bl2awg2e5nqj23',
    authorPHID: 'PHID-USER-ohzzl3maortma7y4znpb',
    date: '1683562276',
    boardPHID: 'PHID-PROJ-izvc7cprybmkosiqgfra'
  },
  {
    taskID: '280999',
    toColumnPHID: 'PHID-PCOL-e3ppe2bl2awg2e5nqj23',
    fromColumnPHID: 'PHID-PCOL-tkydygo6emex7eml3xno',
    authorPHID: 'PHID-USER-5ewyncd6mpezaymyxfal',
    date: '1619204094',
    boardPHID: 'PHID-PROJ-izvc7cprybmkosiqgfra'
  }
];

const getColumnMovementsForTasksWrapper = async taskIDs => {
    return getColumnMovementsForTasks(
      taskIDs,
      process.env.PHABRICATOR_URL,
      process.env.CONDUIT_TOKEN
    );
}

describe('Column movement transactions', () => {

  // Note: the test below uses "expectedEntries" from the Wikimedia Phabricator instance.
  // For non-Wikimedia testing you'd need to update expectedEntries above and the taskIDs used below.

  test('"receivedEntries" are sorted according to "date" field', async () => {
    const receivedEntries = await getColumnMovementsForTasksWrapper([280999, 148805]);
    // As a time-series order is important
    expect(receivedEntries.sort((a, b) => { return a.date < b.date })).toEqual(receivedEntries);
  });

  test('"receivedEntries" contains all elements from "expectedEntries"', async () => {
    const receivedEntries = await getColumnMovementsForTasksWrapper([280999, 148805]);
    // Note: receivedEntries may contain additional entries as the tasks
    // being tested undergo actions creating new transactions. The
    // comparison function "arrayContaining" should allow for these
    expect(receivedEntries).toEqual(expect.arrayContaining(expectedEntries));
  });

  test('entry found for task created in a column', async () => {
    const receivedEntries = await getColumnMovementsForTasksWrapper([336288]);
    expect(receivedEntries).toEqual(expect.arrayContaining(
      [
        {
          "taskID": "336288",
          "toColumnPHID": "PHID-PCOL-ffqlvs2tl3uxzj3webbw",
          "fromColumnPHID": null,
          "authorPHID": "PHID-USER-ohzzl3maortma7y4znpb",
          "date": "1683654549",
          "boardPHID": "PHID-PROJ-4623b3ohfuknkil7xf5n"
        }
      ]
    ));
  });

})